﻿(function () {
	"use strict";

	angular
		.module("backend.services", ["ngResource"])
		.constant("appConfig", { url: "http://localhost:12430/" });
}());