﻿(function () {
    "use strict";

    angular
		.module("backend.services")
		.factory("orderBackendService", ["$resource", "appConfig", orderBackendService]);


    function orderBackendService($resource, appConfig) {
        return {
            orderCreate: $resource(appConfig.url + "api/ordercreate", null,
                {
                    'update': { method: 'PUT' }
                }),
            orderShow: $resource(appConfig.url + "api/ordershow/:guid")
        };
    }

}());