﻿(function () {
    "use strict";

    angular
		.module("backend.services")
		.factory("productBackendService", ["$resource", "appConfig", productBackendService]);


    function productBackendService($resource, appConfig) {
        return $resource(appConfig.url + "api/products/:id", null,
            {
                'update': { method: 'PUT' }
            });
    }

}());