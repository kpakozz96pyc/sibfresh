﻿(function () {
    "use strict";

    angular
		.module("common.services")
		.service("cartCommonService", ["$rootScope","$cookies", cartCommonService]);


    function cartCommonService($rootScope, $cookies) {
        var service = this;

        service.getAmount = function () {
            return $rootScope.amount;
        }

        service.addToCart = function () {
            $rootScope.amount = Number($rootScope.amount) + 1;
            service.saveCart();
        }

        service.deleteFromCart = function () {
            if ($rootScope.amount > 0)
                $rootScope.amount = Number($rootScope.amount) - 1;
            service.saveCart();
        }

        service.saveCart = function () {
            $cookies.put('amount', Number($rootScope.amount));
        }
        service.clearCart = function () {
            $rootScope.amount = 0;
            service.saveCart();
        }

        service.getPrice = function () {
            return $rootScope.amount * 470;
        }
    }
}());