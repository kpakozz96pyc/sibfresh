﻿(function () {
	"use strict";

	angular
		.module("common.services", ["ngResource"])
		.constant("appConfig", { url: "http://localhost:12430/" });
}());