﻿(function () {
    "use strict";

    var sibFresh = angular.module("sibFresh", ["ngRoute", "ngCookies", "common.services", "backend.services"])
        .run(function ($rootScope, $cookies) {
            $rootScope.amount = $cookies.get('amount') ? $cookies.get('amount') : 0;
        });


    sibFresh.config(['$routeProvider',
	function ($routeProvider) {
	    $routeProvider.when('/blog', {
	        templateUrl: 'app/blog/blogListView.html',
	        controller: 'blogListCtrl'
	    }).
        when('/products', {
            templateUrl: 'app/products/productListView.html',
            controller: 'productListCtrl'
        }).
        when('/delivery', {
            templateUrl: 'app/delivery/deliveryView.html'
        }).
        when('/payment', {
            templateUrl: 'app/payment/paymentView.html'
        }).
        when('/ordercreate', {
            templateUrl: 'app/orders/orderCreateView.html',
            controller: 'orderCreateCtrl'
        }).
        when('/ordershow/:guid', {
            templateUrl: 'app/orders/orderShowView.html',
            controller: 'orderShowCtrl'
        }).
        when('/product/:id', {
            templateUrl: 'app/products/productShowView.html',
            controller: 'productShowCtrl'
        }).
	    otherwise({
	        redirectTo: '/products'
	    });
	}])
}());