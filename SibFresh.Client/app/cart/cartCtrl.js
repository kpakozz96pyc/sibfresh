﻿(function () {
    "use strict";
    angular
        .module("sibFresh")
        .controller("cartCtrl", ["$rootScope","$cookies","cartCommonService", cartCtrl]);

    function cartCtrl($rootScope, $cookies, cartCommonService) {
        var vm = this;

        vm.cartService = function () {return cartCommonService };
    }
}());
