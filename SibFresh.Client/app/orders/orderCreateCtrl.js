﻿(function () {
    "use strict";
    angular
        .module("sibFresh")
        .controller("orderCreateCtrl", ["cartCommonService", "orderBackendService", orderCreateCtrl]);

    function orderCreateCtrl(cartCommonService, orderBackendService) {
        var vm = this;

        vm.message = "";

        vm.cartService = function () {
            return cartCommonService;
        }

        vm.order = {};

        orderBackendService.orderCreate.get(
            function (data) {
                vm.order = data;
            }
        );

        vm.submit = function () {
            vm.order.Count = vm.cartService().getAmount();
            vm.order.$save(
            function (data) {
                vm.cartService().clearCart();
                window.location.href = "#/ordershow/" + data.AlternativeId;
            },
            function (response) {
                if (response.data.ModelState) {
                    for(var key in response.data.ModelState){
                        vm.message += response.data.ModelState[key];
                    }
                    if(response.data.ExceptionMessage)
                        vm.message += response.data.ExceptionMessage
                }
            })
        }

    }
}());
