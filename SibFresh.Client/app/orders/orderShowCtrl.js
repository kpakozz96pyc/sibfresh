﻿(function () {
    "use strict";
    angular
        .module("sibFresh")
        .controller("orderShowCtrl", ["$routeParams","orderBackendService", orderShowCtrl]);

    function orderShowCtrl($routeParams, orderBackendService) {
        var vm = this;
        vm.order = {};
        vm.message = "";

        orderBackendService.orderShow.get({"guid":$routeParams.guid},
            function (data) {
                vm.order = data;
            }
        );

    }
}());
