(function () {
    "use strict";

    angular
        .module("sibFresh")
        .controller("productShowCtrl",["cartCommonService",productShowCtrl]);

    function productShowCtrl(cartCommonService) {
        var vm = this;

        vm.cartService = function () { return cartCommonService; }
    }
}());
