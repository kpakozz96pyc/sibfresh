﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SibFresh.Context.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        Order GetById(int id);
        Order GetByAlternativeId(Guid id);
    }

    internal class OrderRepository : Repository<Order>, IOrderRepository
    {
        internal OrderRepository(SibFreshDatabase database) : base(database)
        {
        }

        public Order GetByAlternativeId(Guid id)
        {
            return new Order();
        }

        public Order GetById(int id)
        {
            return new Order();
        }
    }
}
