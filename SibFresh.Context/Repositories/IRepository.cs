﻿using LinqToDB;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace SibFresh.Context
{
    public interface IRepository<TEntity>
    {
        /// <summary>Выполняет вставку сущности.</summary>
        /// <param name="entity">Сущность для вставки.</param>
        /// <returns>The <see cref="int"/>.</returns>
        int Insert(TEntity entity);

        /// <summary>Выполняет вставку сущности и возвращает значение ее Identity поля.</summary>
        /// <param name="entity">Сущность для вставки.</param>
        /// <returns>Значение Identity поля вставленной сущности.</returns>
        object InsertWithIdentity(TEntity entity);

        /// <summary>Удалить записи с условием.</summary>
        /// <param name="predicate">Условие удаления записей.</param>
        /// <returns>Количество удаленных элементов (todo: может и нет).</returns>
        int Delete(Expression<Func<TEntity, bool>> predicate);
    }

    /// <summary>Репозиторий для сущности типа <see cref="TEntity"/>.</summary>
    /// <typeparam name="TEntity">Тип сущности.</typeparam>
    internal class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly SibFreshDatabase Database;

        public Repository(SibFreshDatabase database)
        {
            Database = database;
        }

        /// <summary>Выполняет вставку сущности.</summary>
        /// <param name="entity">Сущность для вставки.</param>
        /// <returns>Значение Identity поля вставленной сущности.</returns>
        public int Insert(TEntity entity)
        {
            return Database.Insert(entity);
        }

        /// <summary>Выполняет вставку сущности и возвращает значение ее Identity поля.</summary>
        /// <param name="entity">Сущность для вставки.</param>
        /// <returns>Значение Identity поля вставленной сущности.</returns>
        public object InsertWithIdentity(TEntity entity)
        {
            return Database.InsertWithIdentity(entity);
        }

        /// <summary>Удалить записи с условием.</summary>
        /// <param name="predicate">Условие удаления записей.</param>
        /// <returns>Количество удаленных элементов (todo: может и нет).</returns>
        public int Delete(Expression<Func<TEntity, bool>> predicate)
        {
            return Database.Delete(predicate);
        }

        protected ITable<TEntity> Table()
        {
            return Database.GetTable<TEntity>();
        }

        /// <summary>Возвращает контекст для запроса к сущности.</summary>
        /// <returns>The <see cref="IQueryable{TEntity}"/>.</returns>
        protected IQueryable<TEntity> Query()
        {
            return Database.GetTable<TEntity>();
        }
    }
}
