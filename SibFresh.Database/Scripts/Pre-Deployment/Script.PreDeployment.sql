﻿BEGIN TRY
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'DatabasePatches'))
BEGIN

	BEGIN TRANSACTION

	-- Paste references on predeployment patches here


	COMMIT TRANSACTION


	END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE() + '. В строке: ' + CAST(ERROR_LINE() AS  NVARCHAR),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
	
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH