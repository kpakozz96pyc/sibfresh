﻿CREATE TABLE [dbo].[Order_Order]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[FirstName] [nvarchar](250) NOT NULL,
	[LastName] [nvarchar](250) NULL,
	[Patronymic] [nvarchar](250) NULL,
	[PhoneNumber] [nvarchar](20) NOT NULL,
	[Address] [nvarchar](250) NULL,
	[Count] [int] NOT NULL,
	[ModificationDate] [datetime2](7) NOT NULL,
	[CreationDate] [datetime2](7) NOT NULL,
	[AlternativeId] [uniqueidentifier] NOT NULL ,
	[Comment] [nvarchar](2500) NULL
)
