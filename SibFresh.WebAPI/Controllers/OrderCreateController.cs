﻿using System;
using System.Collections.Generic;
using SibFresh.WebAPI.Models;
using System.Web.Http;
using System.Web.Http.OData;
using System.Collections;
using System.Web.Http.Cors;
using System.Linq;
using System.Web.Http.Description;

namespace SibFresh.WebAPI.Controllers
{
    [EnableCors(origins: "http://localhost:20939", headers: "*", methods: "*")]
    public class OrderCreateController : ApiController
    {
        List<OrderModel> repo = new List<OrderModel>();

        // GET: api/Products
        public IHttpActionResult Get()
        {
            OrderModel order = new OrderModel { OrderId = 0 };
            return Ok(order);
        }

        // POST: api/Products
        public IHttpActionResult Post([FromBody]OrderModel order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (order == null)
                return BadRequest("Product cannot be null");
            repo.Add(order);
            return Created<OrderModel>(Request.RequestUri + order.OrderId.ToString(), order);
        }
    }
}
