﻿using System;
using System.Collections.Generic;
using SibFresh.WebAPI.Models;
using System.Web.Http;
using System.Web.Http.OData;
using System.Collections;
using System.Web.Http.Cors;
using System.Linq;
using System.Web.Http.Description;


namespace SibFresh.WebAPI.Controllers
{
    [EnableCors(origins: "http://localhost:20939", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        List<Product> repository = new List<Product> {
            new Product {Id =1, Description = "Описание рыбы 1", Name="Название рыбы 1", Price = 449 },
            new Product {Id =2, Description = "Описание рыбы 2", Name="Название рыбы 2", Price = 450 },
            new Product {Id =3, Description = "Описание рыбы 3", Name="Название рыбы 3", Price = 451 },
            new Product {Id =4, Description = "Описание рыбы 4", Name="Название рыбы 4", Price = 452 }
        };
        // GET: api/Products
        [EnableQuery()]
        [ResponseType(typeof(Product))]
        public IHttpActionResult Get()
        {
            //Todo Переделать с репозиторием
            try
            {
                var products = repository;
                return Ok(products.AsQueryable());
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/Products
        public IEnumerable<Product> Get(string search)
        {
            //Todo Переделать с репозиторием
            var products = repository;
            return products.Where(p => p.Description.Contains(search));
        }

        // GET: api/Products
        public IHttpActionResult Get(int id)
        {
            Product product;
            if (id > 0)
            {
                product = repository.FirstOrDefault(p => p.Id == id);
                if (product == null)
                    return NotFound();
            }
            else
            {
                product = new Product { Id =5};
            }
            return Ok(product);
        }

        // POST: api/Products
        public IHttpActionResult Post([FromBody]Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (product == null)
                return BadRequest("Product cannot be null");
            repository.Add(product); 
            return Created<Product>(Request.RequestUri + product.Id.ToString(), product); 
        }

        // PUT: api/Products/5
        public IHttpActionResult Put(int id, [FromBody]Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            repository.Add(product);
            return Created<Product>(Request.RequestUri + product.Id.ToString(), product);
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
        }
    }
}
