﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace SibFresh.WebAPI.Models
{
    public class OrderModel
    {
        public int OrderId { get; set; }

        [Display(Name = "Номер заказа")]
        public int OrderNumber { get { return OrderId + 1015; } }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Введите корректный емейл")]
        public string Email { get; set; }

        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [Display(Name = "Количество рыб")]
        public int Count { get; set; }

        [Display(Name = "Комментарий к заказу")]
        public string Comment { get; set; }

        public Guid AlternativeId { get; set; }
    }
}