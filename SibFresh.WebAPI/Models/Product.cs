﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SibFresh.WebAPI.Models
{
    public class Product
    {
        [Required]
        public int Id { get; set; }
        [MinLength(6,ErrorMessage ="Error message")]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }
    }
}